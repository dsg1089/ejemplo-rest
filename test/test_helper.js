const mongoose = require('mongoose');

before(done =>{

    mongoose.connect('mongodb://localhost:27017/upc', { 
    useNewUrlParser: true ,
    useUnifiedTopology: true,
    useCreateIndex : true}, (err) => {
        if (err) throw err;
        console.log("\nbase de datos conectada para test");
        done()
    });
})