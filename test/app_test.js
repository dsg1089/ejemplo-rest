const assert = require('assert');
const request = require('supertest')
const app = require('../index');

describe('Test unitarios', ()=>{

    it('Buscar Skynet', done =>{
        request(app)
        .get('/proyectos/skynet')
        .end((err,res)=>{
            assert(res.body.proyectoDB.nombre === "SKYNET")
            done();
        })
    })  

    it('Eliminar proyecto con id: 5ef19cc3b897084c74a6ba07(finalizado)', done =>{
        request(app)
        .delete('/proyectos/5ef19cc3b897084c74a6ba07')
        .end((err,res)=>{
            assert(res.body.error === "proyecto ya está finalizado")
            done();
        })
    })  

})