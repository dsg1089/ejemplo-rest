const mongoose = require('mongoose');
const validarUnicidad = require('mongoose-unique-validator');

let Modelo = mongoose.Schema

const Responsable  = mongoose.model('Responsable',new Modelo());


let estadosValidos = {
    values: ['pendiente', 'en proceso', 'finalizado'],
    message: '{VALUE} no es un estado válido'
};

let proyectoModelo = new Modelo({
    fechaCreacion : {
        type: Date,
        default: Date.now() - 5*60*60*1000
    },
    nombre:{
        type: String,
        required: [true,"Es necesario el nombre"],
        unique: true
    },
    descripcion: {
        type: String
    },
    vencimiento:{
        type: Date,
        required: [true,"El vencimiento es obligatorio"]
    },
    responsables:[
        {   
            type: Modelo.Types.ObjectId, ref: Responsable,
            required: [true,"Se requiere por lo menos un responsable"]
        }
    ],
    estado: {
        type: String,
        default: 'pendiente',
        enum: estadosValidos
    },
    fechaActualizacion:{
        type: Date
    },
})

proyectoModelo.plugin(validarUnicidad,{message: "debe ser único"});

module.exports = mongoose.model('Proyecto', proyectoModelo);