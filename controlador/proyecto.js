const express = require('express');
const Proyecto = require('../modelo/proyecto');
const app = express();

//registrar proyecto
app.post('/proyectos', (req, res) =>{

    try {
        var responsables = JSON.parse(req.body.responsables)       
    } catch (error) {
        return res.status(400).json({error : "Responsables debe ser un arreglo de id's"})
    }

    let nombre = req.body.nombre.toUpperCase()

    let proyecto = new Proyecto({
        nombre,
        descripcion: req.body.descripcion,
        vencimiento: req.body.vencimiento,
        responsables
    });

    proyecto.save((error,proyectoDB) => {
        if (error) return res.status(400).json({error: error.message})
        res.json({
            ok : 'creado correctamente',
            proyecto: proyectoDB    
        });
    });
});


//obtener proyectos
app.get('/proyectos', (req, res) =>{

    Proyecto.find()
        .populate('responsables','nombre')
        .exec((error, proyectos) => {
            if (error) return res.status(400).json({error})
            res.json({proyectos});
        });
});


//modificar proyecto
app.put('/proyectos/:id', (req, res)=> {

    Proyecto.findById(req.params.id, (error, proyectoDB)=>{
        if (error) return res.status(400).json({error});

        if (proyectoDB.estado == 'finalizado') return res.json({error: 'proyecto ya ha sido finalizado'})

        let cambios = {
            fechaActualizacion : Date.now() - 5*60*60*1000,
            estado : req.body.estado
        }

        Proyecto.findByIdAndUpdate(req.params.id, cambios, { new: true, runValidators: true }, (err) => {
            if (err) return res.status(400).json({error : err});
            res.json({ok : 'actualizado correctamente'});
        })
    
    })

});


//borrar proyecto
app.delete('/proyectos/:id', (req, res)=> {

    Proyecto.findById(req.params.id, (error, proyectoDB)=>{
        if (error) return res.status(400).json({error});

        if (proyectoDB.estado != 'pendiente'){
            return res.json({error: `proyecto ya está ${proyectoDB.estado}`})
        }else{
            Proyecto.findByIdAndDelete(req.params.id, (err) => {
                if (err) return res.status(400).json({err})
                res.json({ok : 'eliminado correctamente'});
            });
        }
    })
});


//buscar proyecto
app.get('/proyectos/:nombre', function(req, res) {

    let nombre = req.params.nombre.toUpperCase()
    Proyecto.findOne({nombre})
        .populate('responsables','nombre')
        .exec((error, proyectoDB) => {
            if (error) {
                console.log(error);
                return res.status(400).json({error})}
            if(!proyectoDB) {
                return res.status(400).json({error : 'proyecto no existe'})
            }else{
                res.json({proyectoDB});
            }
        });

});


module.exports = app;