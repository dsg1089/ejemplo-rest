const express = require('express');
const mongoose = require('mongoose');


const app = express();


const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.use(require('./controlador/proyecto'));


mongoose.connect('mongodb://localhost:27017/upc', { 
    useNewUrlParser: true ,
    useUnifiedTopology: true,
    useCreateIndex : true}, (err) => {
        if (err) throw err;
        console.log("bd conectada");
});

app.listen(3000,() => {
    console.log("app online en http://localhost:3000");
});

module.exports = app;